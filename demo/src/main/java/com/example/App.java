package com.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        // VegetarianSandwich vs = VegetarianSandwich();
        ISandwich ists = new TofuSandwich();
        TofuSandwich ts = new TofuSandwich();
        

        if(ts instanceof ISandwich){
            System.out.println("Works");
        }
        else{
            System.out.println("Does not work");
        }

        if(ts instanceof VegetarianSandwich){
            System.out.println("Works");
        }
        else{
            System.out.println("Does not work");
        }

        //ts.addFilling("chicken");

        System.out.println(ts.isVegan());

        ists = (VegetarianSandwich) ists;

        //ists.isVegan();

        VegetarianSandwich cvs = new CaeserSandwich();

        System.out.println(cvs.isVegeterian());
    }
}
