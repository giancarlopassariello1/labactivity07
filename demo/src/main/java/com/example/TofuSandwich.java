package com.example;

public class TofuSandwich extends VegetarianSandwich {

    private String filling;

    public TofuSandwich() {
        this.filling = "Tofu";
    }

    @Override
    public String getProtein() {
        return "Tofu";
    }
}
