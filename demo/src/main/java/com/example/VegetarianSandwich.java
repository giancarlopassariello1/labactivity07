package com.example;

public abstract class VegetarianSandwich implements ISandwich{

    private String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }
        
    public String getFilling(){
        return this.filling;
    }

    public void addFilling(String topping){
        String[] meat = {"chicken", "beef", "fish", "meat", "pork"};
        for (int i = 0; i < meat.length; i++){
            if(topping.equals(meat[i])){
                throw new IllegalArgumentException("Cannot add meat to a vegetarian sandwich");
            }
        }
        this.filling += topping;
    }

    //final
    public boolean isVegeterian(){
        return true;
    }

    public boolean isVegan(){
        String[] s = {"cheese", "egg"};
        for (int i = 0; i < s.length; i++){
            if(this.filling.contains("cheese") && this.filling.contains("egg")){
                return false;
            }
        }
        return true;
    }

    public abstract String getProtein();
}