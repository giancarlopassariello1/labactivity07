package com.example;

public class BLTSandwich implements ISandwich {
    private String filling;
    
    public BLTSandwich() {
        this.filling = "Bacon, lettuce, tomato";
    }

    public void addFilling(String topping) {
        this.filling += topping;
    }

    public String getFilling() {
        return this.filling;
    }

    public boolean isVegeterian() {
        return false;
    }
}
