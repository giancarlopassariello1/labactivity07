package com.example;

public class ConcreteVegetarianSandwich extends VegetarianSandwich {

    public ConcreteVegetarianSandwich() {
        super();
    }

    @Override
    public String getProtein() {
        return "Protein content";
    }
}
