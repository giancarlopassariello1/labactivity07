package com.example;

public class CaeserSandwich extends VegetarianSandwich {

    private String filling;

    public CaeserSandwich(){
        String filling = "Caeser dressing";
        this.filling = filling;
    }

    @Override
    public String getProtein(){
        return "Anchovies";
    }

    @Override
    public boolean isVegeterian(){
        return false;
    }
}
