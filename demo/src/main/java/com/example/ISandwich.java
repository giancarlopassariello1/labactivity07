package com.example;

public interface ISandwich {
    String getFilling();
    void addFilling(String topping);
    boolean isVegeterian();
}
