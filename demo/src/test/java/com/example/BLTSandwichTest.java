package com.example;
import static org.junit.Assert.*;

import org.junit.Test;

public class BLTSandwichTest {
    
    @Test
    public void test_Constructor(){
        BLTSandwich blt = new BLTSandwich();
    }


    @Test
    public void test_AddFilling(){
        String topping = " new topping";
        BLTSandwich blt = new BLTSandwich();
        blt.addFilling(topping);
        assertEquals("Bacon, lettuce, tomato new topping", blt.getFilling());
    }

    @Test
    public void should_return_false_isVegetarian(){
        BLTSandwich blt = new BLTSandwich();
        boolean result = blt.isVegeterian();
        assertFalse(result);
    }

    @Test
    public void test_getFilling(){
        BLTSandwich blt = new BLTSandwich();
        assertEquals("Bacon, lettuce, tomato", blt.getFilling());
    }
}
