package com.example;

import static org.junit.Assert.*;

import org.junit.Test;

public class VegetarianSandwichTest {
    
    @Test
    public void test_Constructor(){
        ConcreteVegetarianSandwich vs = new ConcreteVegetarianSandwich();
    }

    @Test
    public void test_AddFilling(){
        String topping = "new topping";
        ConcreteVegetarianSandwich vs = new ConcreteVegetarianSandwich();
        vs.addFilling(topping);
        assertEquals("new topping", vs.getFilling());
    }

    @Test
    public void testIsVegetarian() {
        ConcreteVegetarianSandwich vs = new ConcreteVegetarianSandwich();
        boolean result = vs.isVegeterian();
        assertTrue(result);
    }

    @Test
    public void test_getFilling(){
        ConcreteVegetarianSandwich vs = new ConcreteVegetarianSandwich();
        assertEquals("", vs.getFilling());
    }
}